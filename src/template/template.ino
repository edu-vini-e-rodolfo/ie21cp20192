#include <Arduino.h>
#include <LiquidCrystal.h>

const int rs = 12, en = 11, d4 = 5 ,d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4 ,d5, d6, d7);

int mq3_pino_analogico = A0;
void setup(){
  lcd.begin(16,2);

}
void loop(){
  lcd.clear();
  lcd.setCursor(0,1);
  int valor_mq3 = analogRead(mq3_pino_analogico);

  if(valor_mq3 < 700){
    lcd.setCursor(0,0);
    lcd.print("Taxa Alcool: ");
    lcd.print(valor_mq3);
    lcd.setCursor(0,1);
    lcd.print("Voce ta suave!!!!!");
  }
  else{
    lcd.setCursor(0,0);
    lcd.print("Taxa Alcool: ");
    lcd.print(valor_mq3);
    lcd.setCursor(0,1);
    lcd.print("NAO DIRIJA!!!");
  }
  delay(1000);
}