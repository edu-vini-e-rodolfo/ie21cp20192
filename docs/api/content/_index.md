## Projeto de criação de um Bafometro

Bem vindo ao nosso projeto!

# Introdução:
Neste trabalho vamos criar um bafometro utilizando principalmente arduino e um sensor MQ-3, para identificar presença de alcool no usuario.

# Objetivos:
Identificar se o usuário esta embriagado, assim sabendo se está
impossibilitado de dirigir.

# Materiais utilizados:
* Arduino mega 2560
* Display 16x2
* Jumpers
* Sensor MQ-3
* Proto board
* Potenciometro 10K
* Notebook com software arduino IDE
* Papel umidecido com alcool

# Resultados:
Não conseguimos obter todos os resultados esperados por conta do sensor utilizado, mas foram de certa forma satisfatorios e suficientes. Conseguimos saber se há presença de alcool porem sem o nivel de alcool.

# Desafios encontrados:.
A parte onde tivemos maiores dificuldades foi na hora da programaçao do bafometro, por termos trabalhado muito pouco com arduino previamente.

