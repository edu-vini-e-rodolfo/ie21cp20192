# Introdução
Neste trabalho vamos criar um bafometro utilizando principalmente arduino e um sensor MQ-3, para identificar presença de alcool no usuario.

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
|Rodolfo Koch|rodolfokoch|
|Vinicios Frezza|vinifrezza|
|Eduardo Giordani|EduGg|

# Documentação

A documentação do projeto pode ser acessada pelo link:

https://edu-vini-e-rodolfo.gitlab.io/ie21cp20192/

Video do projeto pode ser acessado pelo link:

https://www.youtube.com/watch?v=THrh0iK41Kc&feature=youtu.be

As imagens do projeto podem ser encontradas pelo link:

https://ibb.co/album/c5DNGF

